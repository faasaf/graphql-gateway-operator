## GraphQL Gateway Operator

A simple ansible operator to deploy a [graphql-gateway](https://gitlab.com/faasaf/graphql-gateway) whenever a custom resource was created.

### Getting Started

It is required to have a running openfaas instance and a kubernetes cluster running with a `~/.kube/config` file present. The commands below assume microk8s is present with openfaas/openfaas helm chart installed in namespace `openfaas`.

```bash
git clone https://gitlab.com/faasaf/graphql-gateway-operator.git
cd graphql-gateway-operator/
make deploy
```

Go to your openfaas dashboard and deploy the `haveibeenpwned` function from the function store.

Now we can create our custom resource in `public-api.yml`:

```yaml
apiVersion: apps.operator.faasaf.io/v1alpha1
kind: GraphQlGateway
metadata:
  name: public-api
  namespace: public
spec:
  defaultBackend: https://gateway.openfaas.svc.cluster.local:8080
  # map:
  #   haveibeenpwned: https://some.external.faaas:31112
  schema: |
    type FoundResponse {
      found: String
    }

    type Query {
      haveibeenpwned(body: String!): FoundResponse
    }
```

```bash
kubectl create ns public
kubectl apply -f ./public-api.yml
```

This will create a new instance of graphql-gateway in the apropriate namespace.
It is up to you to use the provided services to make the gateway available to clients.

You can view the deployment locally with

```bash
kubectl -n public port-forward service/public-api-graphql-gateway 8081:80
```

#### Clean-up

```bash
kubectl delete -f ./public-api.yml
make VERSION undeploy
```

#### CRD GraphQlGateway

There are 3 keys in the CRD spec, 2 of which are required `defaultBackend` and `schema`.
The `defaultBackend` should be provided to let the `graphql-gateway` instance know, which backend to
proxy queries to.
The last and optional parameters is `map`: `map` contains a key-value store to hold different backends for queries or mutations.
